﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;

namespace SOQLHelper
{
    /// <summary>Defines extensions methods related to <see cref="SoqlComparisonOperators"/>, <see cref="SoqlLogicalOperators"/>, and <see cref="SoqlOptionalOperators"/>.</summary>
    public static class SoqlOperatorsExtensions
    {
        /// <summary>Get Description value for TEnum  of type <see cref="SoqlComparisonOperators"/>, <see cref="SoqlLogicalOperators"/>, and <see cref="SoqlOptionalOperators"/>.</summary>
        public static string ToOperator<TEnum>(this TEnum operation) where TEnum : struct, IComparable, IFormattable, IConvertible
        {
            if (typeof(TEnum) != typeof(SoqlComparisonOperators) &&
                typeof(TEnum) != typeof(SoqlLogicalOperators) &&
                typeof(TEnum) != typeof(SoqlOptionalOperators)) throw new ArgumentException("TEnum must be a SoqlOptionalOperators, SoqlComparisonOperators or SoqlLogicalOperators type");

            FieldInfo fieldInfo = typeof(TEnum).GetField(operation.ToString(CultureInfo.InvariantCulture));

            return fieldInfo.GetAttributeValue((DescriptionAttribute d) => d.Description);
        }
    }
}