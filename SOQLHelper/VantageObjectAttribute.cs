﻿using System;

namespace SOQLHelper
{
    [AttributeUsage(AttributeTargets.Class)]
    public class VantageObjectAttribute: Attribute
    {
        #region fields

        #endregion

        #region Constructor

        public VantageObjectAttribute(string vantageObjectName)
        {
            Name = vantageObjectName;
        }

        #endregion

        #region Properties

        public string Name { get; }

        #endregion
    }
}
