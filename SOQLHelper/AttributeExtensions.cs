﻿using System;
using System.Linq;
using System.Reflection;

namespace SOQLHelper
{
    public static class AttributeExtensions
    {
        /// <summary> Gets the value of the specified attribute off the give class type </summary>
        /// <typeparam name="TAttribute"> The attribute to inspect</typeparam>
        /// <typeparam name="TValue"> The return type. </typeparam>
        /// <param name="type"> The type provided by 'this'.</param>
        /// <param name="valueSelector"> A function that describes how to retrive the value.</param>
        /// <returns> The requested value.</returns>
        /// <example>
        ///     string name = typeof(MyClass).GetAttributeValue((DomainNameAttribute dna) => dna.Name);
        /// </example>
        public static TValue GetAttributeValue<TAttribute, TValue>(
            this Type type,
            Func<TAttribute, TValue> valueSelector)
            where TAttribute : Attribute
        {
            TAttribute attribute = type.GetCustomAttributes(typeof(TAttribute), true).FirstOrDefault() as TAttribute;

            return attribute != null ? valueSelector(attribute) : default(TValue);
        }

        /// <summary> Gets the value of the specified attribute off the given <see cref="PropertyInfo"/></summary>
        /// <typeparam name="TAttribute"> The attribute to inspect</typeparam>
        /// <typeparam name="TValue"> The return type. </typeparam>
        /// <param name="propertyInfo"> The <see cref="PropertyInfo"/> provided by 'this'.</param>
        /// <param name="valueSelector"> A function that describes how to retrive the value.</param>
        /// <returns> The requested value.</returns>
        /// <example>
        ///     string name = propertyInfo.GetAttributeValue((JsonPropertyAttribute j) => j.PropertyName);
        /// </example>
        public static TValue GetAttributeValue<TAttribute, TValue>(
            this PropertyInfo propertyInfo,
            Func<TAttribute, TValue> valueSelector)
            where TAttribute : Attribute
        {
            TAttribute attribute = propertyInfo.GetCustomAttributes(typeof(TAttribute), true).FirstOrDefault() as TAttribute;

            return attribute != null ? valueSelector(attribute) : default(TValue);
        }

        /// <summary> Gets the value of the specified attribute off the given <see cref="FieldInfo"/></summary>
        /// <typeparam name="TAttribute"> The attribute to inspect</typeparam>
        /// <typeparam name="TValue"> The return type. </typeparam>
        /// <param name="fieldInfo"> The <see cref="FieldInfo"/> provided by 'this'.</param>
        /// <param name="valueSelector"> A function that describes how to retrive the value.</param>
        /// <returns> The requested value.</returns>
        /// <example>
        ///     string name = fieldInfo.GetAttributeValue((DescriptionAttribute d) => d.Description);
        /// </example>
        public static TValue GetAttributeValue<TAttribute, TValue>(
            this FieldInfo fieldInfo,
            Func<TAttribute, TValue> valueSelector)
            where TAttribute : Attribute
        {
            TAttribute attribute = fieldInfo.GetCustomAttributes(typeof(TAttribute), true).FirstOrDefault() as TAttribute;

            return attribute != null ? valueSelector(attribute) : default(TValue);
        }
    }
}
