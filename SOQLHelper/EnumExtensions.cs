﻿using System;
using System.ComponentModel;
using System.Linq;

namespace SOQLHelper
{
    /// <summary>Contains extension methods of the <see cref="Enum"/> class.</summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Returns the text description of this member according to <see cref="DescriptionAttribute.Description" /> if present. Otherwise, ToString().
        /// </summary>
        /// <param name="enumeration">The enum member.</param>
        /// <returns>The description as a string.</returns>
        public static string ToDescription(this Enum enumeration)
        {
            Type enumType = enumeration.GetType();
            DescriptionAttribute description = enumType.GetField(enumeration.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false)
                .FirstOrDefault() as DescriptionAttribute;
            return (description == null ? enumeration.ToString() : description.Description);
        }
    }
}
