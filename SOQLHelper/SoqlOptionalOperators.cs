﻿using System.ComponentModel;

namespace SOQLHelper
{
    /// <summary>Defines a specializes optional operators for SOQL queries.</summary>
    public enum SoqlOptionalOperators
    {
        /// <summary>Defines a SOQL optional operators value for Order by.</summary>
        [Description("ORDER BY")]
        OrderBy,

        /// <summary>Defines a SOQL optional operators value for Limit.</summary>
        [Description("LIMIT")]
        Limit,

        /// <summary>Defines a SOQL optional operators value for Offset.</summary>
        [Description("OFFSET")]
        Offset
    }
}
