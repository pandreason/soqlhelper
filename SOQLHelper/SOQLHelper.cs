﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace SOQLHelper
{
    /// <summary>Defines a SOQL string building utilities.</summary>
    public static class SoqlHelper
    {
        #region MyRegion

        private const string SPACE = " ";
        private const string DESC = "DESC";
        private const string ASC = "ASC";
        private const char SINGLE_COMMA = ',';

        private const char SINGLE_QUOTE = '\'';
        private const char DOUBLE_QUOTE = '"';
        private const char SINGLE_SLASH = '\\';

        private const string ESCAPED_SINGLE_QUOTE = "\\\'";
        private const string ESCAPED_DOUBLE_QUOTE = "\\\"";
        private const string ESCAPED_SINGLE_SLASH = "\\\\";
        private const int MAXIMUM_LENGTH_OF_WHERE_CLAUSE_QUERY = 4000;

        private static readonly KeyValuePair<char, string>[] _sanitizeDictionary =
        {
            new KeyValuePair<char, string>(SINGLE_SLASH, ESCAPED_SINGLE_SLASH),
            new KeyValuePair<char, string>(SINGLE_QUOTE, ESCAPED_SINGLE_QUOTE),
            new KeyValuePair<char, string>(DOUBLE_QUOTE, ESCAPED_DOUBLE_QUOTE)
        };

        #endregion

        #region Soql builders

        /// <summary> Generate the <see cref="SoqlLogicalOperators.Select"/> SOQL from the <paramref name="soqlObjectType"/></summary>
        /// <param name="soqlObjectType">Object <see cref="Type"/> to select from.</param>
        /// <returns>Generated SOQL <see cref="string"/> from the <paramref name="soqlObjectType"/>.</returns>
        public static string SelectFrom(Type soqlObjectType)
        {
            PropertyInfo[] properties = soqlObjectType.GetProperties();

            string returnString = SoqlLogicalOperators.Select.ToOperator();
            List<string> propertyNames = new List<string>();

            foreach (PropertyInfo propertyInfo in properties)
                propertyNames.AddRange(propertyInfo.GetSoqlPropertyName());

            string propertyClause = string.Join($",{SPACE}", propertyNames.Where(x => x != null));

            returnString +=
                $"{SPACE}{propertyClause}{SPACE}{SoqlLogicalOperators.From.ToOperator()}{SPACE}{soqlObjectType.GetAttributeValue((VantageObjectAttribute v) => v.Name)}";

            return returnString;
        }

        /// <summary> 
        /// Generate the <see cref="SoqlLogicalOperators.Where"/> SOQL from the <paramref name="property"/>, <paramref name="operation"/> and <paramref name="constraint"/> then add to the <paramref name="input"/> SOQL string.
        /// </summary>
        public static string Where(this string input, PropertyInfo property, SoqlComparisonOperators operation, string constraint)
        {
            string currentSoqlStatement = $"{input}{SoqlLogicalOperators.Where.ToOperator().BuildComparisonClause(property, operation, constraint)}";

            ValidateWhereClauseLength(currentSoqlStatement);

            return currentSoqlStatement;
        }

        /// <summary> 
        /// Generate the <see cref="SoqlLogicalOperators.Where"/> SOQL from the <paramref name="propertyName"/>, <paramref name="operation"/> and <paramref name="constraint"/> then add to the <paramref name="input"/> SOQL string.
        /// </summary>
        public static string Where(this string input, string propertyName, SoqlComparisonOperators operation, string constraint)
        {
            string currentSoqlStatement = $"{input}{SoqlLogicalOperators.Where.ToOperator().BuildComparisonClause(propertyName, operation, constraint)}";

            ValidateWhereClauseLength(currentSoqlStatement);

            return currentSoqlStatement;
        }

        /// <summary> 
        /// Generate the <see cref="SoqlLogicalOperators.And"/> SOQL from the <paramref name="property"/>, <paramref name="operation"/> and <paramref name="constraint"/> then add to the <paramref name="input"/> SOQL string.
        /// </summary>
        public static string And(this string input, PropertyInfo property, SoqlComparisonOperators operation, string constraint)
        {
            string currentSoqlStatement = $"{input}{SoqlLogicalOperators.And.ToOperator().BuildComparisonClause(property, operation, constraint)}";

            ValidateWhereClauseLength(currentSoqlStatement);

            return currentSoqlStatement;
        }

        /// <summary> 
        /// Generate the <see cref="SoqlLogicalOperators.And"/> SOQL from the <paramref name="propertyName"/>, <paramref name="operation"/> and <paramref name="constraint"/> then add to the <paramref name="input"/> SOQL string.
        /// </summary>
        public static string And(this string input, string propertyName, SoqlComparisonOperators operation,
            string constraint)
        {
            string currentSoqlStatement = $"{input}{SoqlLogicalOperators.And.ToOperator().BuildComparisonClause(propertyName, operation, constraint)}";

            ValidateWhereClauseLength(currentSoqlStatement);

            return currentSoqlStatement;
        }

        /// <summary> 
        /// Generate the <see cref="SoqlLogicalOperators.And"/> SOQL from the <paramref name="propertyName"/>, <paramref name="operation"/> and <paramref name="constraint"/> then add to the <paramref name="input"/> SOQL string.
        /// </summary>
        public static string And(this string input, string propertyName, SoqlComparisonOperators operation,
            SoqlComparisonValues constraint)
        {
            string currentSoqlStatement = $"{input}{SoqlLogicalOperators.And.ToOperator().BuildComparisonClause(propertyName, operation, constraint.ToDescription())}";

            ValidateWhereClauseLength(currentSoqlStatement);

            return currentSoqlStatement;
        }

        /// <summary> 
        /// Generate the <see cref="SoqlLogicalOperators.Or"/> SOQL from the <paramref name="property"/>, <paramref name="operation"/> and <paramref name="constraint"/> then add to the <paramref name="input"/> SOQL string.
        /// </summary>
        public static string Or(this string input, PropertyInfo property, SoqlComparisonOperators operation, string constraint)
        {
            string currentSoqlStatement = $"{input}{SoqlLogicalOperators.Or.ToOperator().BuildComparisonClause(property, operation, constraint)}";

            ValidateWhereClauseLength(currentSoqlStatement);

            return currentSoqlStatement;
        }

        /// <summary> 
        /// Generate the <see cref="SoqlLogicalOperators.Or"/> SOQL from the <paramref name="propertyName"/>, <paramref name="operation"/> and <paramref name="constraint"/> then add to the <paramref name="input"/> SOQL string.
        /// </summary>
        public static string Or(this string input, string propertyName, SoqlComparisonOperators operation, string constraint)
        {
            string currentSoqlStatement = $"{input}{SoqlLogicalOperators.Or.ToOperator().BuildComparisonClause(propertyName, operation, constraint)}";

            ValidateWhereClauseLength(currentSoqlStatement);

            return currentSoqlStatement;
        }

        /// <summary> 
        /// Generate the <see cref="SoqlLogicalOperators.Not"/> SOQL from the <paramref name="property"/>, <paramref name="operation"/> and <paramref name="constraint"/> then add to the <paramref name="input"/> SOQL string.
        /// </summary>
        public static string Not(this string input, PropertyInfo property, SoqlComparisonOperators operation, string constraint)
        {
            string currentSoqlStatement = $"{input}{SoqlLogicalOperators.Not.ToOperator().BuildComparisonClause(property, operation, constraint)}";

            ValidateWhereClauseLength(currentSoqlStatement);

            return currentSoqlStatement;
        }

        /// <summary> 
        /// Generate the <see cref="SoqlLogicalOperators.Not"/> SOQL from the <paramref name="propertyName"/>, <paramref name="operation"/> and <paramref name="constraint"/> then add to the <paramref name="input"/> SOQL string.
        /// </summary>
        public static string Not(this string input, string propertyName, SoqlComparisonOperators operation, string constraint)
        {
            string currentSoqlStatement = $"{input}{SoqlLogicalOperators.Not.ToOperator().BuildComparisonClause(propertyName, operation, constraint)}";

            ValidateWhereClauseLength(currentSoqlStatement);

            return currentSoqlStatement;
        }

        /// <summary> 
        /// Generate the <see cref="SoqlOptionalOperators.OrderBy"/> SOQL from the <paramref name="propertiesToOrderBy"/>, if its is in <paramref name="descendingOrder"/> then add to the <paramref name="input"/> SOQL string.
        /// </summary>
        public static string OrderBy(this string input, IEnumerable<PropertyInfo> propertiesToOrderBy,
            bool descendingOrder = false)
        {
            List<string> properties = new List<string>();

            foreach (PropertyInfo propertyInfo in propertiesToOrderBy)
                properties.AddRange(propertyInfo.GetSoqlPropertyName());

            string propertyClause = string.Join($",{SPACE}", properties);

            string orderByClause = $"{input}{SPACE}{SoqlOptionalOperators.OrderBy.ToOperator()}{SPACE}{propertyClause}";

            if (descendingOrder) orderByClause += $"{SPACE}{DESC}";
            else orderByClause += $"{SPACE}{ASC}";

            ValidateWhereClauseLength(orderByClause);

            return orderByClause;
        }

        /// <summary> 
        /// Generate the <see cref="SoqlOptionalOperators.OrderBy"/> SOQL from the <paramref name="propertiesToOrderBy"/>, if its is in <paramref name="descendingOrder"/> then add to the <paramref name="input"/> SOQL string.
        /// </summary>
        public static string OrderBy(this string input, IEnumerable<string> propertiesToOrderBy,
            bool descendingOrder = false)
        {
            string propertyClause = string.Join($",{SPACE}", propertiesToOrderBy);

            string orderByClause = $"{input}{SPACE}{SoqlOptionalOperators.OrderBy.ToOperator()}{SPACE}{propertyClause}";

            if (descendingOrder) orderByClause += $"{SPACE}{DESC}";
            else orderByClause += $"{SPACE}{ASC}";

            ValidateWhereClauseLength(orderByClause);

            return orderByClause;
        }

        /// <summary> 
        /// Generate the <see cref="SoqlOptionalOperators.Limit"/> SOQL from the <paramref name="numberToLimit"/> then add to the <paramref name="input"/> SOQL string.
        /// </summary>
        public static string Limit(this string input, int numberToLimit)
        {
            string currentSoqlStatement = $"{input}{SPACE}{SoqlOptionalOperators.Limit.ToOperator()}{SPACE}{numberToLimit}";

            ValidateWhereClauseLength(currentSoqlStatement);

            return currentSoqlStatement;
        }

        /// <summary> 
        /// Generate the <see cref="SoqlOptionalOperators.Offset"/> SOQL from the <paramref name="numberToOffset"/> then add to the <paramref name="input"/> SOQL string.
        /// </summary>
        public static string Offset(this string input, int numberToOffset)
        {
            string currentSoqlStatement = $"{input}{SPACE}{SoqlOptionalOperators.Offset.ToOperator()}{SPACE}{numberToOffset}";

            ValidateWhereClauseLength(currentSoqlStatement);

            return currentSoqlStatement;
        }

        /// <summary> 
        /// Generate the <see cref="SoqlComparisonOperators"/> SOQL for any matching the <paramref name="constraint"/> then add to the <paramref name="input"/> SOQL string.
        /// </summary>
        public static string Any(this string input, IEnumerable<PropertyInfo> properties,
            SoqlComparisonOperators comparisonOperator, string constraint)
        {
            List<string> propertyNames = new List<string>();

            foreach (PropertyInfo propertyInfo in properties)
                propertyNames.AddRange(propertyInfo.GetSoqlPropertyName());

            string currentSoqlStatement = $"{input}{SPACE}{SoqlLogicalOperators.And.ToOperator()}{SPACE}({string.Join($"{SPACE}{SoqlLogicalOperators.Or.ToOperator()}{SPACE}", propertyNames.Select(x => $"{x}{SPACE}{comparisonOperator.ToOperator()}{SPACE}'{SanitizeConstraint(constraint)}'"))})";

            ValidateWhereClauseLength(currentSoqlStatement);

            return currentSoqlStatement;
        }

        #endregion

        #region Utility methods

        private static List<string> GetSoqlPropertyName(this PropertyInfo property)
        {
            string propertyName = property.GetAttributeValue((DataMemberAttribute dm) => dm.Name);

            List<string> propertyNames = new List<string>();

            if (!string.IsNullOrWhiteSpace(property.PropertyType.GetAttributeValue((VantageObjectAttribute v) => v.Name)))
                foreach (PropertyInfo propertyInfo in property.PropertyType.GetProperties())
                    propertyNames.AddRange(propertyInfo.GetSoqlPropertyName(propertyName));
            else
                propertyNames.Add(property.GetAttributeValue((DataMemberAttribute dm) => dm.Name));

            return propertyNames;
        }

        private static List<string> GetSoqlPropertyName(this PropertyInfo property, string relationshipName)
        {
            string propertyName = property.GetAttributeValue((DataMemberAttribute dm) => dm.Name);

            if (string.IsNullOrWhiteSpace(propertyName))
                return new List<string>();

            return new List<string> { $"{relationshipName}.{propertyName}" };
        }

        private static string BuildComparisonClause(this string clauseHeader, PropertyInfo property,
            SoqlComparisonOperators operation, string constraint)
        {
            //TODO: This needs to be parametrized so that we can choose the property to build the clause on
            string propertyName = property.GetSoqlPropertyName().First();

            return clauseHeader.BuildComparisonClause(propertyName, operation, constraint);
        }

        private static string BuildComparisonClause(this string clauseHeader, string propertyName,
            SoqlComparisonOperators operation, string constraint)
        {
            string constraintClause;

            switch (operation)
            {
                case SoqlComparisonOperators.In:
                case SoqlComparisonOperators.NotIn:
                    constraintClause = $"({SanitizeCompoundConstraints(constraint)})";
                    break;
                default:
                    constraintClause = $"{SanitizeConstraint(constraint)}";
                    break;
            }

            return $"{SPACE}{clauseHeader}{SPACE}{propertyName}{SPACE}{operation.ToOperator()}{SPACE}{constraintClause}";
        }

        private static void ValidateWhereClauseLength(string currentSoqlStatement)
        {

            int originalLengthOfInClause = currentSoqlStatement.Contains("WHERE") ? currentSoqlStatement.Substring(currentSoqlStatement.LastIndexOf("WHERE", StringComparison.OrdinalIgnoreCase)).Length : 0;

            if (originalLengthOfInClause > MAXIMUM_LENGTH_OF_WHERE_CLAUSE_QUERY) throw new InvalidOperationException($@"The 'WHERE' clause in the SOQL statement has exceeded 4000 characters. SOQL Statement: {currentSoqlStatement}");
        }

        private static string SanitizeConstraint(string constraint)
        {
            constraint = constraint.Trim();
            return constraint.StartsWith($"{SINGLE_QUOTE}") && constraint.EndsWith($"{SINGLE_QUOTE}")
                ? $"'{_sanitizeDictionary.Aggregate(constraint.TrimStart(SINGLE_QUOTE).TrimEnd(SINGLE_QUOTE), (current, pair) => current.Replace($"{pair.Key}", pair.Value))}'"
                : $"{_sanitizeDictionary.Aggregate(constraint, (current, pair) => current.Replace($"{pair.Key}", pair.Value))}";
        }

        private static string SanitizeCompoundConstraints(string constraint)
        {
            List<string> workingConstraints = constraint.Split(SINGLE_COMMA).ToList();
            List<string> sanitizedConstraints = workingConstraints.Select(SanitizeConstraint).ToList();

            string sanitizedResponse = sanitizedConstraints.Count > 1
                ? string.Join($"{SINGLE_COMMA}{SPACE}", sanitizedConstraints.ToArray())
                : sanitizedConstraints.FirstOrDefault();

            return sanitizedResponse;
        }

        #endregion
    }
}
