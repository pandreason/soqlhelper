﻿using System.ComponentModel;

namespace SOQLHelper
{
    /// <summary>Defines a specializes logical operators for SOQL queries.</summary>
    public enum SoqlLogicalOperators
    {
        /// <summary>Defines a SOQL logical operators value for Select.</summary>
        [Description("SELECT")]
        Select,

        /// <summary>Defines a SOQL logical operators value for From.</summary>
        [Description("FROM")]
        From,

        /// <summary>Defines a SOQL logical operators value for Where.</summary>
        [Description("WHERE")]
        Where,

        /// <summary>Defines a SOQL logical operators value for And.</summary>
        [Description("AND")]
        And,

        /// <summary>Defines a SOQL logical operators value for Or.</summary>
        [Description("OR")]
        Or,

        /// <summary>Defines a SOQL logical operators value for Not.</summary>
        [Description("NOT")]
        Not
    }
}