﻿using System.ComponentModel;

namespace SOQLHelper
{
    /// <summary>Defines a specializes comparisons operators for SOQL queries.</summary>
    public enum SoqlComparisonOperators
    {
        /// <summary>Defines a SOQL comparison operators value for Equals.</summary>
        [Description("=")]
        Equals,

        /// <summary>Defines a SOQL comparison operators value for Not Equals.</summary>
        [Description("!=")]
        NotEquals,

        /// <summary>Defines a SOQL comparison operators value for Less Than.</summary>
        [Description("<")]
        LessThan,

        /// <summary>Defines a SOQL comparison operators value for Less Than Or Equal.</summary>
        [Description("<=")]
        LessOrEqual,

        /// <summary>Defines a SOQL comparison operators value for Greater Than.</summary>
        [Description(">")]
        GreaterThan,

        /// <summary>Defines a SOQL comparison operators value for Greater Than Or Equal.</summary>
        [Description(">=")]
        GreaterOrEqual,

        /// <summary>Defines a SOQL comparison operators value for Like.</summary>
        [Description("LIKE")]
        Like,

        /// <summary>Defines a SOQL comparison operators value for In.</summary>
        [Description("IN")]
        In,

        /// <summary>Defines a SOQL comparison operators value for Not In.</summary>
        [Description("NOT IN")]
        NotIn
    }
}
