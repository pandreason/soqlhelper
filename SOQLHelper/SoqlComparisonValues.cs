﻿using System.ComponentModel;

namespace SOQLHelper
{
    /// <summary>Defines a specializes comparison values for SOQL queries.</summary>
    public enum SoqlComparisonValues
    {
        /// <summary>Defines a SOQL comparison value for True.</summary>
        [Description("TRUE")]
        True,

        /// <summary>Defines a SOQL comparison value for FALSE.</summary>
        [Description("FALSE")]
        False,

        /// <summary>Defines a SOQL comparison value for NULL.</summary>
        [Description("NULL")]
        IsNull,

    }
}