﻿using SOQLHelper;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace CMH.Retail.Gateway.Shared.Tests
{
    [VantageObject("Lead")]
    internal class MySoqlTestObject
    {
        [DataMember(Name = "Property1")]
        public string Property1 { get; set; }
        [DataMember(Name = "Property2")]
        public string Property2 { get; set; }
        [DataMember(Name = "Property3")]
        public string Property3 { get; set; }

        [DataMember(Name = "json_property_4")]
        public string Property4 { get; set; }

        [JsonIgnore]
        public string Ignore { get; set; }

        public string NoJsonProp { get; set; }
    }
}