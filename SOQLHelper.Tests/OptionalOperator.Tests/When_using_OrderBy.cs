﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CMH.Retail.Gateway.Shared.Tests;
using NUnit.Framework;

namespace SOQLHelper.Tests.OptionalOperator.Tests
{
    [TestFixture]
    // ReSharper disable once InconsistentNaming
    public class When_using_OrderBy
    {
        #region fields

        private MySoqlTestObject _testObject;
        private List<PropertyInfo> _properties;
        private List<string> _propertyNames;

            #endregion

        #region Setup

        [SetUp]
        public void Initialize()
        {
            _testObject = new MySoqlTestObject();
            _properties = _testObject.GetType().GetProperties().ToList();
            _propertyNames = _testObject.GetType().GetProperties().Select(x => x.Name).ToList();
        }

        #endregion

        #region Tests

        #region PropertyInfo overload tests

        [Test]
        public void Then_there_is_a_space_before_OrderBy()
        {
            const string input = "a";
            string observedValue = input.OrderBy(_properties);

            Assert.AreEqual(" ", observedValue[observedValue.LastIndexOf(SoqlOptionalOperators.OrderBy.ToOperator(), StringComparison.Ordinal) - 1].ToString());
        }

        [Test]
        public void Then_there_is_a_space_after_OrderBy()
        {
            const string input = "a";
            string observedValue = input.OrderBy(_properties);

            Assert.AreEqual(" ", observedValue[observedValue.LastIndexOf(SoqlOptionalOperators.OrderBy.ToOperator(), StringComparison.Ordinal) + 8].ToString());
        }

        [Test]
        public void Then_the_result_contains_OrderBy()
        {
            const string input = "a";
            string observedValue = input.OrderBy(_properties);

            Assert.IsTrue(observedValue.Contains(SoqlOptionalOperators.OrderBy.ToOperator()));
        }
        
        [Test]
        [TestCase(null, TestName = nameof(Then_the_result_contains_the_correct_sort_indicator) + "_when_the_default_descending_order_value_is_used.")]
        [TestCase(false, TestName = nameof(Then_the_result_contains_the_correct_sort_indicator) + "_when_the_descending_order_value_is_false.")]
        [TestCase(true, TestName = nameof(Then_the_result_contains_the_correct_sort_indicator) + "_when_the_descending_order_value_is_true.")]
        public void Then_the_result_contains_the_correct_sort_indicator(bool? descendingOrder)
        {
            const string input = "a";
            string observedValue = input.OrderBy(_properties, descendingOrder.HasValue && descendingOrder.Value);

            if(descendingOrder.HasValue && descendingOrder.Value)
                Assert.IsTrue(observedValue.Contains("DESC"));
            else
                Assert.IsTrue(observedValue.Contains("ASC"));
        }

        #endregion

        #region string overload tests

        [Test]
        public void Then_there_is_a_space_before_OrderBy_when_using_the_string_overload()
        {
            const string input = "a";
            string observedValue = input.OrderBy(_propertyNames);

            Assert.AreEqual(" ", observedValue[observedValue.LastIndexOf(SoqlOptionalOperators.OrderBy.ToOperator(), StringComparison.Ordinal) - 1].ToString());
        }

        [Test]
        public void Then_there_is_a_space_after_OrderBy_when_using_the_string_overload()
        {
            const string input = "a";
            string observedValue = input.OrderBy(_propertyNames);

            Assert.AreEqual(" ", observedValue[observedValue.LastIndexOf(SoqlOptionalOperators.OrderBy.ToOperator(), StringComparison.Ordinal) + 8].ToString());
        }

        [Test]
        public void Then_the_result_contains_OrderBy_when_using_the_string_overload()
        {
            const string input = "a";
            string observedValue = input.OrderBy(_propertyNames);

            Assert.IsTrue(observedValue.Contains(SoqlOptionalOperators.OrderBy.ToOperator()));
        }

        [Test]
        [TestCase(null, TestName = nameof(Then_when_using_the_string_overload_the_result_contains_the_correct_sort_indicator) + "_when_the_default_descending_order_value_is_used.")]
        [TestCase(false, TestName = nameof(Then_when_using_the_string_overload_the_result_contains_the_correct_sort_indicator) + "_when_the_descending_order_value_is_false.")]
        [TestCase(true, TestName = nameof(Then_when_using_the_string_overload_the_result_contains_the_correct_sort_indicator) + "_when_the_descending_order_value_is_true.")]
        public void Then_when_using_the_string_overload_the_result_contains_the_correct_sort_indicator(bool? descendingOrder)
        {
            const string input = "a";
            string observedValue = input.OrderBy(_propertyNames, descendingOrder.HasValue && descendingOrder.Value);

            if (descendingOrder.HasValue && descendingOrder.Value)
                Assert.IsTrue(observedValue.Contains("DESC"));
            else
                Assert.IsTrue(observedValue.Contains("ASC"));
        }

        #endregion

        #endregion
    }
}
