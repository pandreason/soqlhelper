﻿using System;
using CMH.Retail.Gateway.Shared.Tests;
using NUnit.Framework;

namespace SOQLHelper.Tests
{
    [TestFixture]
    // ReSharper disable once InconsistentNaming
    public class When_using_SelectFrom
    {
        #region fields

        private Type _testType;

        #endregion

        #region Setup

        [SetUp]
        public void Initialize()
        {
            _testType = typeof(MySoqlTestObject);
        }

        #endregion

        #region Tests

        [Test]
        public void The_SelectFrom_methods_result_should_start_with_Select()
        {
            string observedValue = SoqlHelper.SelectFrom(_testType);

            Assert.IsTrue(observedValue.StartsWith(SoqlLogicalOperators.Select.ToOperator()));
        }

        [Test]
        public void The_SelectFrom_methods_result_should_contain_From()
        {
            string observedValue = SoqlHelper.SelectFrom(_testType);

            Assert.IsTrue(observedValue.Contains(SoqlLogicalOperators.From.ToOperator()));
        }

        [Test]
        public void The_SelectFrom_methods_result_should_end_with_the_Vantage_object_name()
        {
            string observedValue = SoqlHelper.SelectFrom(_testType);

            Assert.IsTrue(observedValue.Contains("Lead"));
        }

        [Test]
        public void The_SelectFrom_methods_result_should_contain_JSON_property_names_for_properties_decorated_with_the_attribute()
        {
            string observedValue = SoqlHelper.SelectFrom(_testType);

            Assert.IsTrue(observedValue.Contains("json_property_4"));
        }

        [Test]
        public void The_SelectFrom_methods_result_should_contain_all_the_names_of_the_supplied_objects_properties()
        {
            string observedValue = SoqlHelper.SelectFrom(_testType);

            Assert.IsTrue(observedValue.Contains("Property1"));
            Assert.IsTrue(observedValue.Contains("Property2"));
            Assert.IsTrue(observedValue.Contains("Property3"));
        }

        [Test]
        public void The_SelectFrom_methods_result_should_not_contain_json_ignore_or_non_decorated_properties()
        {
            string observedValue = SoqlHelper.SelectFrom(_testType);

            Assert.IsFalse(observedValue.Contains("Ignore"));
            Assert.IsFalse(observedValue.Contains("NoJsonProp"));
        }

        #endregion
    }
}
