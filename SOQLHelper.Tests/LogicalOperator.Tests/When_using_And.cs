﻿using System;
using System.Reflection;
using System.Runtime.Serialization;
using CMH.Retail.Gateway.Shared.Tests;
using NUnit.Framework;

namespace SOQLHelper.Tests.LogicalOperator.Tests
{
    [TestFixture]
    // ReSharper disable once InconsistentNaming
    public class When_using_And
    {
        #region fields

        private const string CONSTRAINT = "'CONSTRAINT'";
        private MySoqlTestObject _testObject;
        private PropertyInfo _property4Info;
        private string _propertyName;

        #endregion

        #region Setup

        [SetUp]
        public void Initialize()
        {
            _testObject = new MySoqlTestObject();
            _property4Info = _testObject.GetType().GetProperty(nameof(_testObject.Property4));
            _propertyName = _property4Info.GetAttributeValue((DataMemberAttribute jp) => jp.Name);
        }

        #endregion

        #region Tests

        #region PropertyInfo overload tests

        [Test]
        public void Then_there_is_a_space_before_And()
        {
            const string input = "a";
            string observedValue = input.And(_property4Info, SoqlComparisonOperators.Equals, CONSTRAINT);

            Assert.AreEqual(" ", observedValue[observedValue.LastIndexOf(SoqlLogicalOperators.And.ToOperator(), StringComparison.Ordinal) - 1].ToString());
        }

        [Test]
        public void Then_there_is_a_space_after_And()
        {
            const string input = "a";
            string observedValue = input.And(_property4Info, SoqlComparisonOperators.Equals, CONSTRAINT);

            Assert.AreEqual(" ", observedValue[observedValue.LastIndexOf(SoqlLogicalOperators.And.ToOperator(), StringComparison.Ordinal) + 3].ToString());
        }

        [Test]
        public void Then_the_result_contains_And()
        {
            const string input = "a";
            string observedValue = input.And(_property4Info, SoqlComparisonOperators.Equals, CONSTRAINT);

            Assert.IsTrue(observedValue.Contains(SoqlLogicalOperators.And.ToOperator()));
        }

        [Test]
        public void Then_the_result_contains_the_property_name()
        {
            const string input = "a";
            string observedValue = input.And(_property4Info, SoqlComparisonOperators.Equals, CONSTRAINT);

            Assert.IsTrue(observedValue.Contains("json_property_4"));
        }

        [Test]
        [TestCase(SoqlComparisonOperators.Equals, TestName = nameof(Then_the_result_contains) + "_the_equals_operator_when_using_equals")]
        [TestCase(SoqlComparisonOperators.GreaterOrEqual, TestName = nameof(Then_the_result_contains) + "_the_greater_or_equals_operator_when_using_greater_or_equals")]
        [TestCase(SoqlComparisonOperators.GreaterThan, TestName = nameof(Then_the_result_contains) + "_the_greater_than_operator_when_using_greater_than")]
        [TestCase(SoqlComparisonOperators.In, TestName = nameof(Then_the_result_contains) + "_the_IN_operator_when_using_IN")]
        [TestCase(SoqlComparisonOperators.LessOrEqual, TestName = nameof(Then_the_result_contains) + "_the_less_or_equal_operator_when_using_less_or_equal")]
        [TestCase(SoqlComparisonOperators.LessThan, TestName = nameof(Then_the_result_contains) + "_the_less_than_operator_when_using_less_than")]
        [TestCase(SoqlComparisonOperators.Like, TestName = nameof(Then_the_result_contains) + "_the_like_operator_when_using_like")]
        [TestCase(SoqlComparisonOperators.NotEquals, TestName = nameof(Then_the_result_contains) + "_the_not_equals_operator_when_using_not_equals")]
        [TestCase(SoqlComparisonOperators.NotIn, TestName = nameof(Then_the_result_contains) + "_the_not_in_operator_when_using_not_in")]
        public void Then_the_result_contains(SoqlComparisonOperators operation)
        {
            const string input = "a";
            string observedValue = input.And(_property4Info, operation, CONSTRAINT);

            Assert.IsTrue(observedValue.Contains(operation.ToOperator()));
        }

        [Test]
        [TestCase(SoqlComparisonOperators.In, TestName = "And_the_comparison_operator_is_IN_" + nameof(Then_the_result_contains_parenthasis))]
        [TestCase(SoqlComparisonOperators.NotIn, TestName = "And_the_comparison_operator_is_NOT_IN_" + nameof(Then_the_result_contains_parenthasis))]
        public void Then_the_result_contains_parenthasis(SoqlComparisonOperators operation)
        {
            const string input = "a";
            string observedValue = input.And(_property4Info, operation, CONSTRAINT);

            Assert.IsTrue(observedValue.Contains(operation.ToOperator()));
            Assert.IsTrue(observedValue.Contains($"({CONSTRAINT})"), $"{observedValue}");
        }

        [Test]
        public void Then_the_result_contains_the_constraint_value()
        {
            const string input = "a";
            string observedValue = input.And(_property4Info, SoqlComparisonOperators.Equals, CONSTRAINT);

            Assert.IsTrue(observedValue.Contains(CONSTRAINT));
        }

        #endregion

        #region string overload tests

        [Test]
        public void Then_there_is_a_space_before_And_when_using_string_overload()
        {
            const string input = "a";
            string observedValue = input.And(_propertyName, SoqlComparisonOperators.Equals, CONSTRAINT);

            Assert.AreEqual(" ", observedValue[observedValue.LastIndexOf(SoqlLogicalOperators.And.ToOperator(), StringComparison.Ordinal) - 1].ToString());
        }

        [Test]
        public void Then_there_is_a_space_after_And_when_using_string_overload()
        {
            const string input = "a";
            string observedValue = input.And(_propertyName, SoqlComparisonOperators.Equals, CONSTRAINT);

            Assert.AreEqual(" ", observedValue[observedValue.LastIndexOf(SoqlLogicalOperators.And.ToOperator(), StringComparison.Ordinal) + 3].ToString());
        }

        [Test]
        public void Then_the_result_contains_And_when_using_string_overload()
        {
            const string input = "a";
            string observedValue = input.And(_propertyName, SoqlComparisonOperators.Equals, CONSTRAINT);

            Assert.IsTrue(observedValue.Contains(SoqlLogicalOperators.And.ToOperator()));
        }

        [Test]
        public void Then_the_result_contains_the_property_name_when_using_string_overload()
        {
            const string input = "a";
            string observedValue = input.And(_propertyName, SoqlComparisonOperators.Equals, CONSTRAINT);

            Assert.IsTrue(observedValue.Contains("json_property_4"));
        }

        [Test]
        [TestCase(SoqlComparisonOperators.Equals, TestName = nameof(Then_when_using_string_overload_the_result_contains) + "_the_equals_operator_when_using_equals")]
        [TestCase(SoqlComparisonOperators.GreaterOrEqual, TestName = nameof(Then_when_using_string_overload_the_result_contains) + "_the_greater_or_equals_operator_when_using_greater_or_equals")]
        [TestCase(SoqlComparisonOperators.GreaterThan, TestName = nameof(Then_when_using_string_overload_the_result_contains) + "_the_greater_than_operator_when_using_greater_than")]
        [TestCase(SoqlComparisonOperators.In, TestName = nameof(Then_when_using_string_overload_the_result_contains) + "_the_IN_operator_when_using_IN")]
        [TestCase(SoqlComparisonOperators.LessOrEqual, TestName = nameof(Then_when_using_string_overload_the_result_contains) + "_the_less_or_equal_operator_when_using_less_or_equal")]
        [TestCase(SoqlComparisonOperators.LessThan, TestName = nameof(Then_when_using_string_overload_the_result_contains) + "_the_less_than_operator_when_using_less_than")]
        [TestCase(SoqlComparisonOperators.Like, TestName = nameof(Then_when_using_string_overload_the_result_contains) + "_the_like_operator_when_using_like")]
        [TestCase(SoqlComparisonOperators.NotEquals, TestName = nameof(Then_when_using_string_overload_the_result_contains) + "_the_not_equals_operator_when_using_not_equals")]
        [TestCase(SoqlComparisonOperators.NotIn, TestName = nameof(Then_when_using_string_overload_the_result_contains) + "_the_not_in_operator_when_using_not_in")]
        public void Then_when_using_string_overload_the_result_contains(SoqlComparisonOperators operation)
        {
            const string input = "a";
            string observedValue = input.And(_propertyName, operation, CONSTRAINT);

            Assert.IsTrue(observedValue.Contains(operation.ToOperator()));
        }

        [Test]
        [TestCase(SoqlComparisonOperators.In, TestName = "And_the_comparison_operator_is_IN_" + nameof(Then_when_using_string_overload_the_result_contains_parenthasis))]
        [TestCase(SoqlComparisonOperators.NotIn, TestName = "And_the_comparison_operator_is_NOT_IN_" + nameof(Then_when_using_string_overload_the_result_contains_parenthasis))]
        public void Then_when_using_string_overload_the_result_contains_parenthasis(SoqlComparisonOperators operation)
        {
            const string input = "a";
            string observedValue = input.And(_propertyName, operation, CONSTRAINT);

            Assert.IsTrue(observedValue.Contains(operation.ToOperator()));
            Assert.IsTrue(observedValue.Contains($"({CONSTRAINT})"));
        }

        [Test]
        [TestCase(SoqlComparisonOperators.In, TestName = "And_the_comparison_operator_is_IN_" + nameof(Then_when_using_string_overload_the_result_contains_parenthasis))]
        [TestCase(SoqlComparisonOperators.NotIn, TestName = "And_the_comparison_operator_is_NOT_IN_" + nameof(Then_when_using_string_overload_the_result_contains_parenthasis))]
        public void Then_when_using_string_overload_the_result_is_correctly_sanitized_using_simple_items(SoqlComparisonOperators operation)
        {
            const string input = "a";
            string inClauseParams = "'1', '2', 'hamburger'";
            string expectedResults = $"{input} AND json_property_4 {operation.ToOperator()} ({inClauseParams})";
            string observedValue = input.And(_propertyName, operation, inClauseParams);
            Assert.AreEqual(observedValue, expectedResults, $"expected: {expectedResults} actual: {observedValue}", null );
            Assert.IsTrue(observedValue.Contains(operation.ToOperator()));
            Assert.IsTrue(observedValue.Contains($"({inClauseParams})"));
        }

        [Test]
        [TestCase(SoqlComparisonOperators.In, TestName = "And_the_comparison_operator_is_IN_" + nameof(Then_when_using_string_overload_the_result_contains_parenthasis))]
        [TestCase(SoqlComparisonOperators.NotIn, TestName = "And_the_comparison_operator_is_NOT_IN_" + nameof(Then_when_using_string_overload_the_result_contains_parenthasis))]
        public void Then_when_using_string_overload_the_result_is_correctly_sanitized_using_complex_items(SoqlComparisonOperators operation)
        {
            const string input = "a";
            string inClauseParams = "'00Q1b000\'001PXnPEAW','00Q1b000\\001PXnAEAW','00Q1b000001\"j4J7EAI','00Q1b000001j2yiEAA','00Q1b000\\001j2ysEAA'";
            string sanitizedClauseParams = "'00Q1b000\\\'001PXnPEAW', '00Q1b000\\\\001PXnAEAW', '00Q1b000001\\\"j4J7EAI', '00Q1b000001j2yiEAA', '00Q1b000\\\\001j2ysEAA'";
            string expectedResults = $"{input} AND json_property_4 {operation.ToOperator()} ({sanitizedClauseParams})";
            string observedValue = input.And(_propertyName, operation, inClauseParams);
            Assert.AreEqual(observedValue, expectedResults, $"expected: {expectedResults} actual: {observedValue}", null);
            Assert.IsTrue(observedValue.Contains(operation.ToOperator()));
            Assert.IsTrue(observedValue.Contains($"({sanitizedClauseParams})"));
        }
        
        [Test]
        public void Then_the_result_contains_the_constraint_value_when_using_string_overload()
        {
            const string input = "a";
            string observedValue = input.And(_propertyName, SoqlComparisonOperators.Equals, CONSTRAINT);

            Assert.IsTrue(observedValue.Contains(CONSTRAINT));
        }

        #endregion

        #endregion
    }
}
